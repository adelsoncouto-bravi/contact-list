# Lista de contatos

Crie uma API REST que armazene pessoas e seus contatos. Uma pessoa pode ter múltiplos
contatos como telefone, e-mail ou whatsapp. A API deve permitir criar, atualizar, obter
e apague as pessoas e os contatos.

# Compilar
Set as variáveis de ambiente, ajustandos seus valores

```sh
export jwt_secret=senhaJWT
export jwt_expiration=8640000
export spring_security_user_name='admin@admin.org'
export spring_security_user_password=admin
export spring_datasource_url='jdbc:mysql://[host]:3306/contato?serverTimezone=America/Sao_Paulo&Unicode=yes&characterEncoding=UTF-8'
export spring_datasource_username=root
export spring_datasource_password=asdfg
export spring_jpa_hibernate_ddl_auto=create
export url_externa='http://localhost:8080'
```

Execute

```sh
./mvnw spring-boot:run
```

# Uso

Acesse o swagger http://[host]:8080/swagger-ui.html

Crie o usuário usando POST para /usuarios

Faça o login conforme swagger

Pegue o token recebido no Header

Envie o token nas próximas requisições no header Authorization 

