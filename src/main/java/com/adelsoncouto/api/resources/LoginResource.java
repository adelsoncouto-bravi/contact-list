package com.adelsoncouto.api.resources;

import com.adelsoncouto.api.model.Usuario;
import com.adelsoncouto.api.security.CredenciaisDTO;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;

/**
 * LoginResource
 */
@RestController
@RequestMapping(value = "/")
public class LoginResource {

  // TODO login fake

  // TODO logout fake

  /**
   * Fake, apenas para fins de documentação
   * 
   * @param aCredencial
   * @return
   */
  @ApiOperation("Login.")
  @PostMapping("/login")
  public ResponseEntity<Usuario> login(@RequestBody CredenciaisDTO aCredencial) {
    throw new IllegalStateException("fake, apenas para fins de documentação");
  }

  /**
   * Fake, apenas para fins de documentação
   */
  @ApiOperation("Logout.")
  @PostMapping("/logout")
  public void logout() {
    throw new IllegalStateException("fake, apenas para fins de documentação");
  }

}
