package com.adelsoncouto.api.resources;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.adelsoncouto.api.dto.TelefoneDTO;
import com.adelsoncouto.api.model.Telefone;
import com.adelsoncouto.api.services.TelefoneService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/telefones")
public class TelefoneResource {

	@Value(value = "${api.url.externa}")
	private String urlExterna;

	@Autowired
	private TelefoneService serviceTelefone;

	@ApiOperation("Busca por ID")
	@RequestMapping(value = "/{aId}", method = RequestMethod.GET)
	public ResponseEntity<Telefone> findById(@PathVariable Integer aId) {
		return ResponseEntity.ok().body(serviceTelefone.findById(aId));
	}

	@ApiOperation("Busca paginada")
	@RequestMapping(value="/list/{aUsuarioId}",method = RequestMethod.GET)
	public ResponseEntity<Page<Telefone>> findAll(
			@PathVariable Integer aUsuarioId,
	    @RequestParam(value = "page", defaultValue = "0") Integer aPage,
	    @RequestParam(value = "linesPerPage", defaultValue = "24") Integer aLinesPerPage,
	    @RequestParam(value = "orderBy", defaultValue = "id") String aOrderBy,
	    @RequestParam(value = "direction", defaultValue = "ASC") String aDirection) {
		Page<Telefone> list = serviceTelefone.findAll(aUsuarioId, aPage, aLinesPerPage, aOrderBy, aDirection);
		return ResponseEntity.ok().body(list);
	}

	@ApiOperation("Salva")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> save(@Valid @RequestBody TelefoneDTO aEntity) {
		Telefone entity = serviceTelefone.save(aEntity);
		URI uri = ServletUriComponentsBuilder.fromHttpUrl(urlExterna).path("/{id}").buildAndExpand(entity.getId()).toUri();
		return ResponseEntity.created(uri).build();
	}

	@ApiOperation("Atualiza")
	@RequestMapping(value = "/{aId}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@PathVariable Integer aId, @Valid @RequestBody TelefoneDTO aEntity) {
		serviceTelefone.update(aId, aEntity);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation("Deleta")
	@RequestMapping(value = "/{aId}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer aId) {
		serviceTelefone.deleteById(aId);
		return ResponseEntity.noContent().build();
	}
}
