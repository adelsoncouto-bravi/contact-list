package com.adelsoncouto.api.dto;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class TelefoneDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	private String ddi;
	private String ddd;
	private String numero;
	private Boolean temWhatsapp;
	
	private Integer usuario;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TelefoneDTO other = (TelefoneDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer aId) {
		id = aId;
	}

	public String getDdi() {
		return ddi;
	}

	public void setDdi(String aDdi) {
		ddi = aDdi;
	}

	public String getDdd() {
		return ddd;
	}

	public void setDdd(String aDdd) {
		ddd = aDdd;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String aNumero) {
		numero = aNumero;
	}

	public Boolean getTemWhatsapp() {
		return temWhatsapp;
	}

	public void setTemWhatsapp(Boolean aTemWhatsapp) {
		temWhatsapp = aTemWhatsapp;
	}

	public Integer getUsuario() {
		return usuario;
	}

	public void setUsuario(Integer aUsuario) {
		usuario = aUsuario;
	}
	
	
}
