package com.adelsoncouto.api.dto;

import java.io.Serializable;

public class UsuarioDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@javax.validation.constraints.Email
	private String email;
	private String senha;
	private String nome;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String aEmail) {
		email = aEmail;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String aSenha) {
		senha = aSenha;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String aNome) {
		nome = aNome;
	}
	
}
