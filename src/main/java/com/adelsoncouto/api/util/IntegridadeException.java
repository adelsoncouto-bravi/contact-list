package com.adelsoncouto.api.util;

public class IntegridadeException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IntegridadeException(String msg) {
		super(msg);
	}

}
