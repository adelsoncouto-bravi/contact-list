package com.adelsoncouto.api.util;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;

@Component
public class HeaderExposureFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(
			ServletRequest aRequest, 
			ServletResponse aResponse, 
			FilterChain aChain)
			throws IOException, ServletException {

		HttpServletResponse res = (HttpServletResponse) aResponse;
		res.addHeader("access-control-expose-headers", "location");
		aChain.doFilter(aRequest, aResponse);
	}

	@Override
	public void destroy() {
	}
}
