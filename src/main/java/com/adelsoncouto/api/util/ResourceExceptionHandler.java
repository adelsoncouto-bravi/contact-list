package com.adelsoncouto.api.util;

import java.time.ZonedDateTime;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.adelsoncouto.api.security.AuthorizationException;

@ControllerAdvice
public class ResourceExceptionHandler {

	@ExceptionHandler(NaoEncontratoException.class)
	public ResponseEntity<StandardError> objectNotFound(NaoEncontratoException aException, HttpServletRequest aRequest) {

		StandardError err = new StandardError(ZonedDateTime.now(BR.ZONE_BRASILIA), HttpStatus.NOT_FOUND.value(),
				"Não encontrado", aException.getMessage(), aRequest.getRequestURI());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(err);
	}

	@ExceptionHandler(IntegridadeException.class)
	public ResponseEntity<StandardError> dataIntegrity(IntegridadeException aException, HttpServletRequest aRequest) {

		StandardError err = new StandardError(ZonedDateTime.now(BR.ZONE_BRASILIA), HttpStatus.BAD_REQUEST.value(),
				"Integridade de dados", aException.getMessage(), aRequest.getRequestURI());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(err);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<StandardError> validation(MethodArgumentNotValidException aException, HttpServletRequest aRequest) {

		ValidationError err = new ValidationError(ZonedDateTime.now(BR.ZONE_BRASILIA),
				HttpStatus.UNPROCESSABLE_ENTITY.value(), "Erro de validação", aException.getMessage(), aRequest.getRequestURI());
		for (FieldError x : aException.getBindingResult().getFieldErrors()) {
			err.addError(x.getField(), x.getDefaultMessage());
		}
		return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(err);
	}

	@ExceptionHandler(AuthorizationException.class)
	public ResponseEntity<StandardError> authorization(AuthorizationException aException, HttpServletRequest aRequest) {

		StandardError err = new StandardError(ZonedDateTime.now(BR.ZONE_BRASILIA), HttpStatus.FORBIDDEN.value(),
				"Acesso negado", aException.getMessage(), aRequest.getRequestURI());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body(err);
	}


}
