package com.adelsoncouto.api.util;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.persistence.AttributeConverter;

public class BR implements AttributeConverter<ZonedDateTime, String> {

	public static final DateTimeFormatter TIME = DateTimeFormatter.ofPattern("HH:mm:ss");
	public static final DateTimeFormatter DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	public static final DateTimeFormatter DATE_BR = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	public static final DateTimeFormatter DATE_TIME = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
	public static final DateTimeFormatter DATE_TIME_ISO = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
	public static final ZoneId ZONE_BRASILIA = ZoneId.of("America/Sao_Paulo");

	@Override
	public String convertToDatabaseColumn(ZonedDateTime aAttribute) {
		return aAttribute.format(BR.DATE_TIME_ISO);
	}

	@Override
	public ZonedDateTime convertToEntityAttribute(String aDbData) {
		return ZonedDateTime.parse(aDbData);
	}

}
