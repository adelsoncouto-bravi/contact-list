package com.adelsoncouto.api.util;


public class NaoEncontratoException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public NaoEncontratoException(String aMensagem) {
		super(aMensagem);
	}
	

}
