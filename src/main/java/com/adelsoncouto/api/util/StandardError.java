package com.adelsoncouto.api.util;

import java.io.Serializable;
import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

public class StandardError implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ssXXX", timezone = "America/Sao_Paulo")
	private ZonedDateTime hora;
	private Integer status;
	private String error;
	private String message;
	private String path;

	public StandardError(
			ZonedDateTime aHora, 
			Integer aStatus, 
			String aError, 
			String aMessage, 
			String aPath) {
		super();
		this.hora = aHora;
		this.status = aStatus;
		this.error = aError;
		this.message = aMessage;
		this.path = aPath;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ZonedDateTime getHora() {
		return hora;
	}

	public void setHora(ZonedDateTime aHora) {
		hora = aHora;
	}
}