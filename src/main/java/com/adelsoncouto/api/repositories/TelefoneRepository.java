package com.adelsoncouto.api.repositories;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adelsoncouto.api.model.Telefone;

@Repository
public interface TelefoneRepository extends JpaRepository<Telefone, Integer> {

	@Transactional(readOnly = true)
	@Query(nativeQuery = true, value = "SELECT * FROM telefone WHERE usuario_id = :usuario", countQuery = "SELECT COUNT(1) FROM telefone WHERE usuario_id = :usuario")
	Page<Telefone> findFromUsuario(Pageable aPageRequest, @Param(value="usuario") Integer aUsuarioId);
}