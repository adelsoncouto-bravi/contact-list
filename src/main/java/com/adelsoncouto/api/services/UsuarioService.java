package com.adelsoncouto.api.services;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.adelsoncouto.api.dto.UsuarioDTO;
import com.adelsoncouto.api.model.Usuario;
import com.adelsoncouto.api.repositories.UsuarioRepository;
import com.adelsoncouto.api.util.NaoEncontratoException;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository repositoryUsuario;

	public Usuario findById(Integer aId) {
		return repositoryUsuario.findById(aId).orElseThrow(() -> new NaoEncontratoException(aId.toString()));
	}

	public Usuario save(Usuario aUsuario) {
		aUsuario.setId(null);
		return repositoryUsuario.save(aUsuario);
	}

	public Usuario update(Usuario aUsuario) {
		repositoryUsuario.findById(aUsuario.getId()).orElseThrow(() -> new NaoEncontratoException("Não encontrado"));
		return repositoryUsuario.save(aUsuario);
	}

	public void deleteById(Integer aId) {
		Usuario entity = repositoryUsuario.findById(aId).orElse(null);
		if (entity != null) {
			repositoryUsuario.deleteById(entity.getId());
		}
	}

	public Page<Usuario> findAll(Integer aPage, Integer aLinesPerPage, String aOrderBy, String aDirection) {
		PageRequest pageRequest = PageRequest.of(aPage, aLinesPerPage, Direction.valueOf(aDirection), aOrderBy);
		return repositoryUsuario.findAll(pageRequest);
	}

	public Usuario save(@Valid UsuarioDTO aDTO) {
		Usuario usuario = new Usuario();
		usuario.setEmail(aDTO.getEmail());
		usuario.setNome(aDTO.getNome());
		usuario.setSenha(aDTO.getSenha());
		return save(usuario);
	}

	public void update(Integer aId, @Valid UsuarioDTO aDTO) {
		Usuario usuario = repositoryUsuario.findById(aId).orElse(null);
		if(usuario == null ) {
			throw new NaoEncontratoException("Usuário não encontrado");
		}
		usuario.setEmail(aDTO.getEmail());
		usuario.setNome(aDTO.getNome());
		usuario.setSenha(aDTO.getSenha());
		repositoryUsuario.save(usuario);
	}


}