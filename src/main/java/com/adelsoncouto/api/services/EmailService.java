package com.adelsoncouto.api.services;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.adelsoncouto.api.dto.EmailDTO;
import com.adelsoncouto.api.model.Email;
import com.adelsoncouto.api.model.Usuario;
import com.adelsoncouto.api.repositories.EmailRepository;
import com.adelsoncouto.api.util.NaoEncontratoException;

@Service
public class EmailService {

	@Autowired
	private EmailRepository repositoryEmail;

	@Autowired
	private UsuarioService serviceUsuario;

	public Email findById(Integer aId) {
		return repositoryEmail.findById(aId).orElseThrow(() -> new NaoEncontratoException(aId.toString()));
	}

	public Email save(Email aEmail) {
		return repositoryEmail.save(aEmail);
	}

	public Email update(Email aEmail) {
		repositoryEmail.findById(aEmail.getId()).orElseThrow(() -> new NaoEncontratoException("Não encontrado"));
		return repositoryEmail.save(aEmail);
	}

	public void deleteById(Integer aId) {
		Email entity = repositoryEmail.findById(aId).orElse(null);
		if (entity != null) {
			repositoryEmail.deleteById(entity.getId());
		}
	}

	public Page<Email> findAll(Integer aUsuarioId, Integer aPage, Integer aLinesPerPage, String aOrderBy,
	    String aDirection) {
		PageRequest pageRequest = PageRequest.of(aPage, aLinesPerPage, Direction.valueOf(aDirection), aOrderBy);
		return repositoryEmail.findFromUsuario(pageRequest, aUsuarioId);
	}

	public void update(Integer aId, @Valid EmailDTO a) {
		Email email = repositoryEmail.findById(aId).orElse(null);

		if (email == null) {
			throw new NaoEncontratoException("E-mail não econtrado");
		}

		Usuario usuario = serviceUsuario.findById(a.getUsuario());

		if (usuario == null) {
			throw new NaoEncontratoException("Usuário não econtrado");
		}

		email.setEmail(a.getEmail());
		email.setUsuario(usuario);

		repositoryEmail.save(email);

	}

	public Email save(@Valid EmailDTO a) {
		Email email = new Email();
		Usuario usuario = serviceUsuario.findById(a.getUsuario());

		if (usuario == null) {
			throw new NaoEncontratoException("Usuário não econtrado");
		}

		email.setEmail(a.getEmail());
		email.setUsuario(usuario);

		return repositoryEmail.save(email);

	}

}