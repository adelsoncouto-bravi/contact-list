package com.adelsoncouto.api.services;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.adelsoncouto.api.dto.TelefoneDTO;
import com.adelsoncouto.api.model.Telefone;
import com.adelsoncouto.api.model.Usuario;
import com.adelsoncouto.api.repositories.TelefoneRepository;
import com.adelsoncouto.api.util.NaoEncontratoException;

@Service
public class TelefoneService {

	@Autowired
	private TelefoneRepository repositoryTelefone;
	
	@Autowired
	private UsuarioService serviceUsuario;

	public Telefone findById(Integer aId) {
		return repositoryTelefone.findById(aId).orElseThrow(() -> new NaoEncontratoException(aId.toString()));
	}

	public Telefone save(Telefone aTelefone) {
		return repositoryTelefone.save(aTelefone);
	}

	public Telefone update(Telefone aTelefone) {
		repositoryTelefone.findById(aTelefone.getId()).orElseThrow(() -> new NaoEncontratoException("Não encontrado"));
		return repositoryTelefone.save(aTelefone);
	}

	public void deleteById(Integer aId) {
		Telefone entity = repositoryTelefone.findById(aId).orElse(null);
		if (entity != null) {
			repositoryTelefone.deleteById(entity.getId());
		}
	}

	public Page<Telefone> findAll(Integer aUsuarioId, Integer aPage, Integer aLinesPerPage, String aOrderBy, String aDirection) {
		PageRequest pageRequest = PageRequest.of(aPage, aLinesPerPage, Direction.valueOf(aDirection), aOrderBy);
		return repositoryTelefone.findFromUsuario(pageRequest, aUsuarioId);
	}

	public void update(Integer aId, @Valid TelefoneDTO aDTO) {
		Telefone telefone = repositoryTelefone.findById(aId).orElse(null);
		if(telefone == null) {
			throw new NaoEncontratoException("Telefone não encontrado");
		}
		Usuario usuario = serviceUsuario.findById(aDTO.getUsuario());
		if(usuario == null) {
			throw new NaoEncontratoException("Usuário não econtrado");
		}
		telefone.setDdd(aDTO.getDdd());
		telefone.setDdi(aDTO.getDdi());
		telefone.setNumero(aDTO.getNumero());
		telefone.setTemWhatsapp(aDTO.getTemWhatsapp());
		telefone.setUsuario(usuario);
		repositoryTelefone.save(telefone);
		
	}

	public Telefone save(@Valid TelefoneDTO aDTO) {
		Usuario usuario = serviceUsuario.findById(aDTO.getUsuario());
		if(usuario == null) {
			throw new NaoEncontratoException("Usuário não econtrado");
		}
		Telefone telefone = new Telefone();
		telefone.setDdd(aDTO.getDdd());
		telefone.setDdi(aDTO.getDdi());
		telefone.setNumero(aDTO.getNumero());
		telefone.setTemWhatsapp(aDTO.getTemWhatsapp());
		telefone.setUsuario(usuario);
		return repositoryTelefone.save(telefone);
	}


}