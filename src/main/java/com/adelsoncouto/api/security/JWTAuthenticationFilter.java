package com.adelsoncouto.api.security;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.adelsoncouto.api.model.Usuario;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;

	private JWTUtil jwtUtil;

	public JWTAuthenticationFilter(AuthenticationManager aAuthenticationManager, JWTUtil aJwtUtil) {
		setAuthenticationFailureHandler(new JWTAuthenticationFailureHandler());
		this.authenticationManager = aAuthenticationManager;
		this.jwtUtil = aJwtUtil;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest aRequest, HttpServletResponse aResponse)
			throws AuthenticationException {

		try {
			CredenciaisDTO creds = new ObjectMapper().readValue(aRequest.getInputStream(), CredenciaisDTO.class);
			
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(creds.getEmail(),
					creds.getSenha(), new HashSet<>());
			Authentication auth = authenticationManager.authenticate(authToken);
			return auth;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest aRequest, HttpServletResponse aResponse, FilterChain aChain,
			Authentication aAuth) throws IOException, ServletException {
		Usuario user = ((Usuario) aAuth.getPrincipal());

		JSONObject code = new JSONObject();
		code.put("id", user.getId());
		code.put("nome", user.getNome());
		code.put("email", user.getUsername());

		String token = jwtUtil.generateToken(code);
		aResponse.addHeader("Authorization", "Bearer " + token);
		aResponse.addHeader("access-control-expose-headers", "Authorization");
	}

	private class JWTAuthenticationFailureHandler implements AuthenticationFailureHandler {

		@Override
		public void onAuthenticationFailure(HttpServletRequest aRequest, HttpServletResponse aResponse,
				AuthenticationException aException) throws IOException, ServletException {
			aResponse.setStatus(401);
			aResponse.setContentType("application/json");
			aResponse.getWriter().append(json());
		}

		private String json() {
			long date = new Date().getTime();
			JSONObject js = new JSONObject();
			js.put("timestamp", date);
			js.put("status", 401);
			js.put("error", "Não autorizado");
			js.put("message", "Email ou senha inválidos");
			js.put("path", "/login");
			return js.toString();
		}
	}
}
