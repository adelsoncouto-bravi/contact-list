package com.adelsoncouto.api.security;

import java.io.Serializable;

public class CredenciaisDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String email;
	private String senha;

	public String getEmail() {
		return email;
	}

	public void setEmail(String aEmail) {
		email = aEmail;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String aSenha) {
		senha = aSenha;
	}

}
