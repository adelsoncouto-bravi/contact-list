package com.adelsoncouto.api.security;


public class AuthorizationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AuthorizationException() {
		super("Não autorizado");
	}


	public AuthorizationException(String aMensagem) {
		super(aMensagem);
	}

}
