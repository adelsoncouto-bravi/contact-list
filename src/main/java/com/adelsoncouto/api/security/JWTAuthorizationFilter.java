package com.adelsoncouto.api.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	private JWTUtil jwtUtil;

	private UserDetailsService userDetailsService;

	public JWTAuthorizationFilter(AuthenticationManager aAuthenticationManager, JWTUtil aJwtUtil,
			UserDetailsService aUserDetailsService) {
		super(aAuthenticationManager);
		this.jwtUtil = aJwtUtil;
		this.userDetailsService = aUserDetailsService;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest aRequest, HttpServletResponse aResponse, FilterChain aChain)
			throws IOException, ServletException {

		String header = aRequest.getHeader("Authorization");

		if (header != null && header.startsWith("Bearer ")) {
			UsernamePasswordAuthenticationToken auth = getAuthentication(header.substring(7));
			if (auth != null) {
				SecurityContextHolder.getContext().setAuthentication(auth);
			}
		}
		aChain.doFilter(aRequest, aResponse);
	}

	private UsernamePasswordAuthenticationToken getAuthentication(String aToken) {
		if (jwtUtil.tokenValido(aToken)) {
			String decode = jwtUtil.getUsername(aToken);
			JSONObject json = new JSONObject(decode);
			UserDetails user = userDetailsService.loadUserByUsername(json.getString("email"));
			return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
		}
		return null;
	}
}
