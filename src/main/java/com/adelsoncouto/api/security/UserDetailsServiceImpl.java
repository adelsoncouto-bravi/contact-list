package com.adelsoncouto.api.security;

import com.adelsoncouto.api.model.Usuario;
import com.adelsoncouto.api.repositories.UsuarioRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UsuarioRepository repositoryUsuario;

	@Override
	public UserDetails loadUserByUsername(String aEmail) throws UsernameNotFoundException {
		Usuario usuario = repositoryUsuario.findByEmail(aEmail).orElseThrow(() -> {
			throw new UsernameNotFoundException(aEmail);
		});
		return usuario;
	}

	public static Usuario authenticated() {
		try {
			return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		} catch (Exception e) {
			return null;
		}
	}
}
